# Final Project

We consider both a top-down and bottom-up approach to forecasting demand (in units) for certain consumer goods. Model implementations based on the DGLM outlined in Berry and West, 2018 [1]. We apply our methods to sales data provided by a hardware wholesaler.



[1] L. Berry, P. Helman, M. West, 2018. Probabilistic forecasting of heterogeneous consumer transaction-sales time series. arXiv:1808.04698
