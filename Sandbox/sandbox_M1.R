# source("z. boot/startup.R")

sales_montly_SKU <- filter(sales_monthly, SKU == 21431)
periods_w_sales <- sales_montly_SKU$Date; sales_SKU <- sales_montly_SKU$Quantity
sales <- rep(0, length(time_periods)); sales[time_periods %in% periods_w_sales] <- sales_SKU

## Feedback
# df_sales <- data.frame(t = 1:T, sales = sales); ggplot(df_sales, aes(t, sales)) + geom_line()

# response time series: 
y <- unlist(unname(sales)); lambda <- log(y + 1)  # shift it


# Components of a DLM
## Trend
Ftrend <- matrix(1); Gtrend <- matrix(1); ntrend <- length(Ftrend)

## AR
p <- 12; iar <- (ntrend + 1):(ntrend + p)
lambda_prior <- rev(tail(head(lambda, 12), p))
lambda_ar <- c(lambda_prior, lambda)
Far <- matrix(lambda_prior, ncol = 1)
Gar <- diag(p)

## Fourier components of seasonal
periods <- 12; rseas <- c(1, 4, 12); pseas <- length(rseas)
nseas <- 2*pseas
Fseas <- matrix(rep(c(1,0), pseas), ncol = 1); Gseas <- matrix(0, nrow = nseas, ncol = nseas)
for (j in 1:pseas) {
  c <- cos(2*pi*rseas[j]/periods); s <- sin(2*pi*rseas[j]/periods); i <- (2*j - 1):(2*j)
  Gseas[i,i] <- matrix(c(c, s, -s, c), nrow = 2, ncol = 2, byrow = TRUE)
}

## Common components
M0 <- unname(as.matrix(read_csv("z. boot/Data/M0.csv", col_names = FALSE)))
ncom <- nrow(M0); icom <- (ntrend + p + nseas + 1):(ntrend + p + nseas + ncom)
Fcom <- matrix(M0[ , 1], ncol = 1)
Gcom <- diag(ncom)

# a_0 <- matrix(c(lambda[1], 
#                 rep(0, p), 
#                 0.09543998, -0.17493264, -0.07284231,  0.09780413, -0.02218247, -0.01400000,
#                 rep(0.1, ncom)), ncol = 1)
a_0 <- matrix(c(lambda[1], 
                -0.42993072, -0.36290701, -0.37340029, -0.20802425, -0.18316697, -0.09436388,
                0.11180952,  0.06829949,  0.07371896, 0.21639644, -0.02189715, -0.06156858, 
                0.16224590, -0.19775251, -0.07529607,  0.09129203, -0.04510936, -0.01400000,
                0.30359172, 0.19360005,  0.22202894,  0.16354479,  0.10971824,  0.61015811,  0.09534760), ncol = 1)

deltrend <- 0.97; delar <- 0.97; delseas <- 0.97; delcom <- 0.97; delvar <- 0.97


# DLM matrices F & G, and discount matrix
F <- rbind(Ftrend, Far, Fseas, Fcom); n <- length(F);  G <- as.matrix(bdiag(Gtrend, Gar, Gseas, Gcom))

disc.mat <- adiag(matrix(deltrend, nrow = ntrend, ncol = ntrend),
                  matrix(delar, nrow = p, ncol = p),
                  matrix(delseas, nrow = nseas, ncol = nseas),
                  matrix(delcom, nrow = ncom, ncol = ncom),
                  pad = 1)

# Save parameters
sf <- sq <- salpha <- sbeta <- sg <- sp <- rep(0, T)
sm <- sa <- sF <- matrix(0, nrow = n, ncol = T)
sC <- sR <- sG <- array(data = 0, dim = c(dim(G), T))

# Initialize parameters
a_t <- a_0
F_t <- F
G_t <- G
R_t <- as.matrix(bdiag(0.09, 0.01*diag(p), 0.0067*diag(nseas), 0.01*diag(ncom))) # think about priors

for (t in 1:T) {
  
  y_t <- y[t]
  
  if (t > 1) {
    
    m_tmin1 <- sm[ , t - 1]; C_tmin1 <- sC[ , , t - 1]; F_tmin1 <- sF[ , t - 1]; F_t <- F_tmin1
    
    a_t <- G_t %*% m_tmin1
    P_t <- G_t %*% C_tmin1 %*% t(G_t)
    R_t <- P_t/disc.mat
    
    F_t[iar]  <- rev(lambda_ar[t:(t + p - 1)])
    F_t[icom] <- M0[ , t]
    
  }
  
  f_t <- as.numeric(t(F_t) %*% a_t)
  q_t <- as.numeric(t(F_t) %*% R_t %*% F_t)/delvar
  
  alpha_t <- newtonRaphson(function(alpha_t) trigamma(alpha_t) - q_t, 1)$root
  # beta_t  <- newtonRaphson(function(beta_t) digamma(alpha_t) - log(beta_t) - f_t, 1)$root
  beta_t  <- newtonRaphson(function(beta_t)  beta_t*exp(f_t) - exp(digamma(alpha_t)), 1)$root
  
  g_t <- digamma(alpha_t + y_t) - log(beta_t + 1)
  p_t <- trigamma(alpha_t + y_t)
  
  m_t <- a_t + R_t %*% F_t * (g_t - f_t)/q_t
  C_t <- R_t - R_t %*% F_t %*% t(F_t) %*% R_t * (1 - p_t/q_t)/q_t
  
  # Save statistics
  sf[t] <- f_t; sq[t] <- q_t; salpha[t] <- alpha_t
  sm[ , t] <- m_t; sa[ , t] <- a_t; sF[ , t] <- F_t
  sC[ , , t] <- C_t; sR[ , , t] <- R_t; sG[ , , t] <- G_t
  
}

df_pred   <- sales_montly_SKU %>% select(Date) %>% mutate(sales = y, units =  exp(sf) - 1)

ggplot(df_pred, aes(t, pred)) +
  geom_line() +
  geom_point(aes(y = y))

ggplot(df_pred, aes(Date, units)) +
  geom_line() + geom_point(aes(y = sales), size = 0.5) +
  labs(y = "Demand (units)") + theme(axis.title.x = element_blank(), axis.title.y = element_blank(),
                                     axis.text.x = element_blank())
