---
title: "STA 642 | Project"
author: "Ricardo Batista (rb313)"
date: "12/6/2018"
output: pdf_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
source("../../z. boot/startup.R")
```


# Abstract  

&nbsp;&nbsp;&nbsp;&nbsp; We consider a top-down and bottom-up approach to forecasting sales demand in units. Model implementation focuses on the DGLM outlined in Berry and West, 2018 [1]. We apply our methods to sales data provided by a hardware wholesaler.  

# Company Overview

&nbsp;&nbsp;&nbsp;&nbsp; Founded in 2009, Corpotool LLC is a small family hardware wholesaler based in South Florida. Its primary customers are retailers in Florida and the Caribbean. Sales over the last 10 years are shown below. Its business model is simple: Corpotool employs salespeople, each of whom is assigned a geographic area. Each salesperson visits all their clients roughly every month.  

```{r, out.width = "450px", echo = FALSE, fig.align = "center"}
knitr::include_graphics("../Plots/Total_sales.jpeg")
# FFBS_M1()
print(t)
```

&nbsp;&nbsp;&nbsp;&nbsp; The Company sells over 10,000 SKUs. As you would imagine, many of these SKUs are discontinued -- either by the manufacturer or Corpotool -- every year. Likewise, many new products are added. The company has recorded approximately 40,000 invoices. 


# Sales Forecasting

&nbsp;&nbsp;&nbsp;&nbsp; Fortunately, Corpotool does not share inventory information with its salesforce. As such, during the sales visit, clients place an order *regardless of whether the items are in stock*. This allows Corpotool to capture the actual demand for the particular item. This demand data is what we will work with (shown in the invoice above).

&nbsp;&nbsp;&nbsp;&nbsp; Corpotool currently relies on a popular simple purchasing algorithm: for any given product, the company must have 60 days worth of merchandise in inventory. This takes into consideration current stock, inbound shipments, and expected demand over the next 15 days. Given this relatively lean inventory management, a purchase decision is made -- either automatically or manually -- on every SKU on a biweekly basis. In this context, long-term sales forecasting is not particularly important.

&nbsp;&nbsp;&nbsp;&nbsp; The 15-day forecast alluded to above is a simple average: take the total units sold over the past 60 days and divide them by 60. That is, Corpotool makes purchases decisions using the average units sold per day over the last 2 months. Moreover, there is no outlier detection: the purchasing manager often discounts the 2-month average if it includes an extraordinary purchase. Given the limited sales history currently employed to forecast sales, perhaps we can produce a more accurate forecast.  

# Project Scope and Limitations

&nbsp;&nbsp;&nbsp;&nbsp; We will limit ourselves to forecasting sales for three SKUs within a single product category. In the process, however, we will outline a hierarchical model that is scalable to all SKUs.

&nbsp;&nbsp;&nbsp;&nbsp; Sales forecasting for a small company entails several non-trivial forecasting challenges. Perhaps the most obvious one is a dearth of data. Many SKUs see monthly sales below 100 units. In addition, salespeople sometimes skip their scheduled client visit -- during which the client would presumably have placed an order --, thus obscuring demand. Another important consideration, particularly when aggregating sales, is discontinued items. In some cases, an SKU is discontinued only to be replaced by a similar item. In other cases, Corpotool stops selling the item altogether, which creates a dip in sales for the department or product category. Lastly, sales forecasting often requires treating recurring and new sales differently, for instance by discounting the later or forecasting them separately.  

&nbsp;&nbsp;&nbsp;&nbsp; Among other complications, Corpotool sells in different countries which may call for separate modeling assumptions. Also, the data pool available to us contains noise in the form of returns and expensive one-off point-of-purchase material installations.  

Naturally, these are issues worth tackling. At this time however, we will focus on fitting a model for each of the three aforementioned SKUs. In the process, we will also fit a model for the product category they belong to. 


# Model

## Exploration and General Model Frameworks

&nbsp;&nbsp;&nbsp;&nbsp; We consider a hierarchical approach to sales forecasting. Hierarchy and categorization are key aspects of hardware sales: merchandise is organized into different departments, which are themselves part of a product category. Thus hierarchy is important not only because it is conducive to inference but also interpretability. We will consider the three sales forecasting approaches: top-down, middle-out, and bottom-up. They each have their benefits and drawbacks, which will be a theme underlying the rest of this paper.

Before we discuss the design pros and cons, we establish the three main hierarchy levels considered in this paper. The highest level of abstraction -- company-level aggregation -- often offers high signal-to-noise ratio relative to lower levels. Consider Figure 1, for instance. There we see a short fast-growth period followed by a level and then what appears like a less dramatic upward trend. At the time, the economy was bouncing back from The Great Recession. Also -- if memory serves me right -- there was a change in management in 2010 (which likely resulted in more frequent sales visits). Taking a closer look of Figure 1 also reveals a yearly pattern, underscored by the troughs at the end of each year. Corpotool adjourns early in December. In any case, the dynamics evidenced by the trend shown in Figure 1 (i.e., 12-month moving average) likely underlie demand across many SKUs.  

&nbsp;&nbsp;&nbsp;&nbsp; The next level of abstraction is department level or, more granular, product category level. A department is composed of different product categories. In hardware, for instance, the gardening department contains the hose, shovel, and rake product categories. Items in a department usually share similar shopping patterns, often subject to the seasons. Therefore, this level often provides a good estimate for such trends. (Product categories consisting of a large number of SKUs can also offer such benefits.)

&nbsp;&nbsp;&nbsp;&nbsp; Despite the allure of forecasting at high levels, the lowest level -- in this case SKU -- is often the best starting point. Granted, as alluded to earlier, this level is often noisy. However, if the demand for an SKU behaves markedly different from its product category, relying on upper-level regressors will lead to disastrous forecasts. Moreover, the SKU level is a natural place to model pricing and promotion.    

## Model Specification

&nbsp;&nbsp;&nbsp;&nbsp; We implement both top-down (i.e., multi-scale DGLM) and bottom-up (i.e., univariate DGLM) models using the framework in Berry and West, 2018 [1]. We limit ourselves to the *rake product category and a single SKU (SKU 21431: 20 Tines Steel Leaf Rake) within it*.  


### General notation

&nbsp;&nbsp;&nbsp;&nbsp; Recall that dynamic regression is defined by the state-space form  

$$
\begin{aligned}
\lambda_t = \textbf{F}_t' \boldsymbol{\theta}_t && \text{where} && \boldsymbol{\theta}_t = \textbf{G}_t \boldsymbol{\theta}_{t - 1} + \boldsymbol{\omega}_t && \text{and} && \boldsymbol{\omega}_t \sim (\boldsymbol{0}, \textbf{W}_t)
\end{aligned}
$$

with elements defined in standard fashion [2]. The Poisson loglinear DGLM deviates from a DLM in that 

$$
\begin{aligned}
y_t \sim Po(\mu_t) && \text{with} && \lambda_t = \log(\mu_t)
\end{aligned}
$$

where $y$ is the outcome of interest. See [1] for the relevant sequential Learning minutia.  

### Top-down

&nbsp;&nbsp;&nbsp;&nbsp; We consider a multi-scale model with two levels (product category and SKU). We do feed the product category level brand effects, as discussed below.

#### Company-wide (i.e., $\mathcal{M}_0$)

&nbsp;&nbsp;&nbsp;&nbsp; As alluded to earlier, we do not fit a model for this level. We simply create an index from company-wide sales by calculating the 12-month moving average of log sales and normalizing it. This produces regressor $\textbf{f}_{0,t}$.  

### Product category (i.e., $\mathcal{M}_1$)

&nbsp;&nbsp;&nbsp;&nbsp; We fit a stochastic volatility DLM for each product category (i.e., $\mathcal{M}_i$) with regression vector and evolution matrix defined by  

$$
\begin{aligned}
\textbf{F}_t' = (\text{f}_{0,t}, 1, 0, 1, 0) && \text{and} && \textbf{G}_t = \text{blockdiag}[1, \textbf{H}_1, \textbf{H}_4]
\end{aligned}
$$

with  

$$
\begin{aligned}
\textbf{H}_j = \begin{pmatrix} \cos(2\pi j/12) & \sin(2\pi j/12) \\
                               -\sin(2\pi j/12) & \cos(2\pi j/12)\end{pmatrix}, && j \in \{1, 4\}
\end{aligned}
$$

We define $\textbf{f}_{1,t}$ as the state vectors associated with this model; we will use these as regressors for the SKU-level model. Lastly, we use discount factors $\delta_1 = 0.87$ and $\delta_2 = 0.99$ for the respective components, as well as $\beta = 0.99$ for the variance. Notice the relatively low regression discount rate. This is by design, with the intent of isolating the seasonal effect as best as possible.

### SKU level (i.e., $\mathcal{M}_{1,i}$)

&nbsp;&nbsp;&nbsp;&nbsp; We fit a Poisson loglinear DGLM with random effects for each SKU with regression vector $\text{f}_{1,t}^{\text{seas}}$ and evolution matrix defined by  

$$
\begin{aligned}
\textbf{F}_t' = (1, \log(\text{price}_t), \text{f}^{seas}_{1,t}) && \text{and} && \textbf{G}_t = \text{I}_6
\end{aligned}
$$

where $\text{f}_{1,t}^{\text{seas}}$ are only the seasonal components of $\text{f}_{1,t}$. We removed the brand effect because the sales history for this SKU does not follow either the product category nor the company-wide trend, as you can tell by relevant graphs. Finally, we use discount factors $\delta_1 = 0.97$ and $\delta_2 = 0.97$ for the respective components, as well as random effects discount factor $\rho = 0.98$. 

## Model Evaluation

&nbsp;&nbsp;&nbsp;&nbsp; We evaluate the model on the rake product category and then SKU 21431.

### Model Evaluation: $\mathcal{M}_{1}$

&nbsp;&nbsp;&nbsp;&nbsp; Notice how closely the trend in aggregate rake sales resembles total sales, with the glaring exception of 2011.

```{r, out.width = "450px", echo = FALSE, fig.align = "center"}
knitr::include_graphics("../Plots/FF_M1.jpeg")
```

The aforementioned anomalous period is reflected in a widening of the brand-wide trend.  

```{r, out.width = "450px", echo = FALSE, fig.align = "center"}
knitr::include_graphics("../Plots/M1_global_coeff.jpeg")
```

We also considered a third seasonal component, namely $j = 3$ but decided against it given the width of its respective smoothed credible interval.  

```{r, out.width = "450px", echo = FALSE, fig.align = "center"}
knitr::include_graphics("../Plots/M1_harm_1.jpeg")
```


```{r, out.width = "450px", echo = FALSE, fig.align = "center"}
knitr::include_graphics("../Plots/M1_harm_2.jpeg")
```


### Model Evaluation: $\mathcal{M}_{1,i}$

&nbsp;&nbsp;&nbsp;&nbsp; The key innovation here is that this model regresses on the state vector of seasonal components produced at the product category level. As we will see after reviewing the univariate case, the seasonal structure of this model appears more consistent than the univariate case. 

```{r, out.width = "450px", echo = FALSE, fig.align = "center"}
knitr::include_graphics("../Plots/Hier_pred.jpeg")
```

Recall that we are regressing on the seasonal coefficients of the product category DLM. With this in mind, the stability of the four coefficients below suggests that demand for SKU 21431 follows a relatively similar seasonal pattern as its product category.  

```{r, out.width = "450px", echo = FALSE, fig.align = "center"}
knitr::include_graphics("../Plots/M2_coef_1.jpeg")
```

```{r, out.width = "450px", echo = FALSE, fig.align = "center"}
knitr::include_graphics("../Plots/M2_coef_2.jpeg")
```

```{r, out.width = "450px", echo = FALSE, fig.align = "center"}
knitr::include_graphics("../Plots/M2_coef_3.jpeg")
```

```{r, out.width = "450px", echo = FALSE, fig.align = "center"}
knitr::include_graphics("../Plots/M2_coef_4.jpeg")
```

### Bottom-up

&nbsp;&nbsp;&nbsp;&nbsp; As alluded to in the top-down approach, SKU 21431 is special in that its sales trajectory is flat, in contrast with both the rakes and company-wide early growth trends. As such, we would expect a bottom-up approach to perform relatively well. We fit a Poisson loglinear DGLM with random effects for each SKU with regression vector $\text{f}_{1,t}^{\text{seas}}$ and evolution matrix defined by  

$$
\begin{aligned}
\textbf{F}_t' = (1, \log(\text{price}_t), 1, 0, 1, 0, 1, 0) && \text{and} && \textbf{G}_t = \text{blockdiag}[1, \textbf{H}_1, \textbf{H}_3, \textbf{H}_4]
\end{aligned}
$$

with  $\textbf{H}_i$ as previously defined. 

```{r, out.width = "450px", echo = FALSE, fig.align = "center"}
knitr::include_graphics("../Plots/FF_single.jpeg")
```

&nbsp;&nbsp;&nbsp;&nbsp; In broad terms, the the bottom-up approach closely resembles the top-down. Similarly to the multi-scale approach, the seasonal components seem to capture the major trends in sales.


```{r, out.width = "450px", echo = FALSE, fig.align = "center"}
knitr::include_graphics("../Plots/single_coef_1.jpeg")
```

```{r, out.width = "450px", echo = FALSE, fig.align = "center"}
knitr::include_graphics("../Plots/single_coef_2.jpeg")
```

Notice the narrow margins of all seasonal components. As alluded to earlier, the 4th harmonic was not distinguishable in the multi-scale approach at the product category level.  

```{r, out.width = "450px", echo = FALSE, fig.align = "center"}
knitr::include_graphics("../Plots/single_coef_3.jpeg")
```

```{r, out.width = "450px", echo = FALSE, fig.align = "center"}
knitr::include_graphics("../Plots/single_coef_4.jpeg")
```

```{r, out.width = "450px", echo = FALSE, fig.align = "center"}
knitr::include_graphics("../Plots/single_coef_5.jpeg")
```


### Model Comparison and Selection

&nbsp;&nbsp;&nbsp;&nbsp; Both models performed relatively similarly and fit the data with no readily apparent bias. As such, the shape of the Q-Q plots below is no surprise. Also expected are the relatively heavier tails.  

```{r, out.width = "450px", echo = FALSE, fig.align = "center"}
knitr::include_graphics("../Plots/Qqplots.jpeg")
```

&nbsp;&nbsp;&nbsp;&nbsp; When evaluating the models with regards to cumulative log likelihood, however, the univariate model outperformed in all instances. This may not be the case in future given the multi-scale model does particularly poorly during the earlier time periods (i.e., 2009, 2010).  

```{r, out.width = "450px", echo = FALSE, fig.align = "center"}
knitr::include_graphics("../Plots/Log_marg_lik.jpeg")
```

&nbsp;&nbsp;&nbsp;&nbsp; Lastly, the summary table below shows the relevant model summaries for the corresponding to the six models in the aforementioned cumulative log likelihood plot. In the multi-scale model the discount rates correspond to the lowest level model (i.e., $\mathcal{M}_{1, i}$). Notice that, for a given set of discount factors, multi-scale models outperform univariate ones, likely due to the more persistent seasonal component we alluded to earlier. Also keep in mind that the "seas" column in the table below stands for the number of seasonal components. That is, we considered both univariate and multi-scale models with varying seasonal components. Moreover, "NA"s appear on the table as a reminder for the reader: the DGLM multi-scale model do not have seasonal components per se -- they regress on the seasonal state vector estimated on by $\mathcal{M}_{1}$.  

```{r, echo=FALSE, fig.align="center", message=FALSE, warning=FALSE, cache=TRUE, fig.width = 10, fig.height = 3}

knitr::kable(data.frame(model = c(rep("univariate", 3),rep("multi-scale", 3)),
           seas = c(3, 3, 2, 2, 2, 3),
           deltrend = c(0.99, 0.97, 0.98, 0.97, 0.99, 0.97),
           delprice = c(0.99, 0.97, 0.99, 0.97, 0.99, 0.99),
           delseas = c(0.99, 0.97, 0.99, NA, NA, NA),
           delregseas = c(NA, NA, NA, 0.98, 0.99,0.99),
           delvar = c(0.99, 0.97, 0.98, 0.97, 0.99, 0.98),
           mlik = c(-687.60, -603.96, -684.78, -707.40, -808.16,-743.897),
           coverage = c(0.45, 0.62, 0.45, 0.53, 0.42, 0.45),
           MSE = c(0.42, 0.53, 0.43, 0.44, 0.37,0.40)))


```

Notice that delvar for the multi-scale case is actually $\rho$ as defined in [1].  

# Future work

&nbsp;&nbsp;&nbsp;&nbsp; Clearly, there are many avenues to pursue, most important of which are (a) evaluating the model on a broader set of SKUs to further validate it, (b) programmatically dealing with discontinued items when creating the product category, and (c) discounting customer orders based on their recurrence level. Ideally we would build an app were the user can enter an SKU and see the sales forecast. This model is a work in progress and it was very rewarding to build -- maybe someday it can inform the purchasing department at Corpotool.  



# References

[1] L. Berry, P. Helman, M. West, 2018. Probabilistic forecasting of heterogeneous consumer transaction-sales time series. arXiv:1808.04698

[2] Prado, R. and West, M. (2010), Time Series: Modelling, Computation & Inference, Chapman &
Hall/CRC Press.
