#' Model
#'  - 3 seasonal

# source("z. boot/startup.R")
sales_montly_SKU1 <- filter(sales_monthly, SKU == 21431)
periods_w_sales1 <- sales_montly_SKU1$Date; sales_SKU1 <- sales_montly_SKU1$Quantity
sales1 <- rep(0, length(time_periods)); sales1[time_periods %in% periods_w_sales1] <- sales_SKU1

## Feedback
# df_sales1 <- data.frame(t = 1:T, sales = sales1); ggplot(df_sales1, aes(t, sales)) + geom_line()

# response time series: 
Y1 <- unlist(unname(sales1)); Y1 <- log(Y1 + 1)  # shift it


sales_montly_SKU2 <- filter(sales_monthly, SKU == 17352)
periods_w_sales2 <- sales_montly_SKU2$Date; sales_SKU2 <- sales_montly_SKU2$Quantity
sales2 <- rep(0, length(time_periods)); sales2[time_periods %in% periods_w_sales2] <- sales_SKU2

## Feedback
# df_sales2 <- data.frame(t = 1:T, sales = sales2); ggplot(df_sales2, aes(t, sales)) + geom_line()

# response time series: 
Y2 <- unlist(unname(sales2)); Y2 <- log(Y2 + 1)  # shift it

Y <- unname(cbind(Y1, Y2))


# Components of a DLM
## Trend
Ftrend <- matrix(1); Gtrend <- matrix(1)

ntrend <- length(Ftrend)
itrend <- 1:ntrend

# Fourier components of seasonal
p <- 12; rseas <- c(1, 3, 12); pseas <- length(rseas)
nseas <- 2*pseas; iseas <- (ntrend + 1):(ntrend + nseas);
Fseas <- matrix(rep(c(1,0), pseas), ncol = 1); Gseas <- matrix(0, nrow = nseas, ncol = nseas)
for (j in 1:pseas) {
  c <- cos(2*pi*rseas[j]/p); s <- sin(2*pi*rseas[j]/p); i <- (2*j - 1):(2*j)
  Gseas[i,i] <- matrix(c(c, s, -s, c), nrow = 2, ncol = 2, byrow = TRUE)
}

# DLM matrices F & G
F <- rbind(Ftrend, Fseas); n <- length(F);  G <- as.matrix(bdiag(Gtrend, Gseas))


# priors p(\theta_1,v|D_0) & 3 discount factors for component DLMs:
nu <- 6; h <- nu + ncol(Y) - 1; D <- h*diag(ncol(Y))    # cov(Y)
Sigma <- D/h
S <- 0.25
Sigma <- D/h
a <- matrix(c(mean(Y1), -0.8, -0.8, 0, 0, -0.2, 0.2,
            c(mean(Y2), -1.014, 0.014, 0.014, 0.014, 0.014, 0.014)), ncol = 2)
R <- as.matrix(bdiag(0.09, 0.0067*diag(nseas)));
deltrend <- 0.8; delseas <- 0.85; delvar <- 0.95

# storage arrays for filtering summaries:
sMt  <- array(0, c(dim(a), T)); sCt <- array(0, c(nrow(F), nrow(F), T))
sdCt <- array(0, c(dim(a), T)); sSt <- array(0, c(ncol(a), ncol(a), T))
snt  <- matrix(0, nrow = 1, ncol = T); sloglik <- matrix(0, nrow = 1, ncol = T)
sf <- matrix(0, nrow = 2, ncol = T); snu <- rep(0, T)
sat  <- array(0, c(dim(a), T)); sRt <- array(0, c(nrow(F), nrow(F), T))

# Now analysis
for (t in 1:T) {
  
  if (t > 1) {
    
    a <- G %*% M
    R <- G %*% C %*% t(G)
    R[itrend, itrend] <- R[itrend,itrend]/deltrend;  
    R[iseas,iseas] <- R[iseas,iseas]/delseas;
    
  }
  
  f <- t(a) %*% F;
  
  y <- t(t(Y[t, ]))
  e <- y - f
  
  h <- delvar*h; nu <- h - ncol(Y) + 1; D <- delvar*D
  
  q <- as.numeric(t(F) %*% R %*% F + S)
  
  # MW records some stuff
  
  A <- (R %*% F)/q
  
  h <- h + 1; nu <- nu + 1; D <- D + (e %*% t(e))/q
  St <- D/h; St <- (St + t(St))/2
  
  M <- a + A %*% t(e)
  C <- R - A%*%t(A)*q
  
  # save posterior at time t:
  sSt[, , t] <- St; sMt[ , , t] <- M; sdCt[ , , t] <- sqrt(diag(C) %*% t(diag(St)))
  sf[, t] <- f; sat[ , , t] <- a; sCt[ , , t] <- C; sRt[ , , t] <- R; snu[t] <- nu
  
}

SKU_1 <- data.frame(t = 1:T, Y = exp(Y1 - 1), pred = exp(sf[1,] - 1))
SKU_2 <- data.frame(t = 1:T, Y = exp(Y2 - 1), pred = exp(sf[2,] - 1))
df_pred <- rbind(SKU_1, SKU_2) %>% mutate(SKU = c(rep(1, T), rep(2, T)))

ggplot(df_pred, aes(t, pred)) + facet_wrap(~SKU, nrow = 2, scales = "free") +
  geom_line() +
  geom_point(aes(y = Y))

# Smoothing
# Sm <- sMt; SC <- sCt; SR <- sRt;  Sa <- sat; SS <- sSt; Snu <- snu

K <- solve(sSt[ , , T]); n <- snt[T]; Mt <- sMt[ , , T]; Ct <- sCt[ , , T]
delta <- c(deltrend, rep(delseas, nrow(Fseas)))
for (t in (T - 1):1) {
  
  K  <- (1 - delvar)*solve(sSt[ , , t]) + delvar*K;                St <- solve(K); sSt[ , , t] <- St
  Mt <- (1 - delvar)*sMt[ , ,t] + delta*Mt;                        sMt[ , , t] <- Mt
  Ct <- (1 - delvar)*sCt[ , ,t] + t(t(delta)) %*% delta %*% Ct;    sCt[ , , t] <- Ct; sdCt[ , , t] <- sqrt(diag(Ct)*t(diag(St)))
  
}

